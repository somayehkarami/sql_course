/* Purpose : create the tables of database AragonPharmacyDB
	Script Date: February 8, 2021
	Developed by : W.L
*/

/* add a statement that specifieds the script
runs in the context of the master database */

-- switch to the Aragon Pharmacy database
use AragonPharmacyDB;
go -- include end of the batch marker

/***** Table No. 1 - Marketing.tblHealthPlan ****/
if OBJECT_ID('Marketing.tblHealthPlan','U')
is not null drop table Marketing.tblHealthPlan
create table Marketing.tblHealthPlan
(
	PlanID		nvarchar(20)	NOT NULL,
	PlanName	nvarchar(50)	NOT NULL,
	Address		nvarchar(50)	NOT NULL,
	City		nvarchar(20)	NOT NULL,
	Province	nchar(2)		NOT NULL,
	PostalCode	nchar(6)		NOT NULL,
	Phone		nvarchar(15)	NOT NULL,
	Days		decimal(5,2)	NOT NULL,
	WebSite		nvarchar(100)	NOT NULL,
	constraint pk_tblHealthPlan primary key clustered (PlanID)
)
;
go

EXEC sp_help 'Marketing.tblHealthPlan'; 
go

/***** Table No. 2 - Marketing.tblHousehold ****/
if OBJECT_ID('Marketing.tblHousehold','U')
is not null drop table Marketing.tblHousehold
create table Marketing.tblHousehold
(
	HouseID		nvarchar(20)	NOT NULL,
	Address		nvarchar(60)	NOT NULL,
	City		nvarchar(15)	NOT NULL,
	Province	nchar(2)		NOT NULL,
	PostalCode	nchar(6)		NOT NULL,
	constraint pk_tblHousehold primary key clustered (HouseID)
)
;
go

EXEC sp_help 'Marketing.tblHousehold'; 
go

/***** Table No. 3 - Marketing.tblCustomer ****/
if OBJECT_ID('Marketing.tblCustomer','U')
is not null drop table Marketing.tblCustomer
create table Marketing.tblCustomer
(
	CustID			nvarchar(10)	NOT NULL,
	PlanID			nvarchar(20)	NOT NULL,
	HouseID			nvarchar(20)	NOT NULL,
	CustFisrt		nvarchar(30)	NOT NULL,
	CustLast		nvarchar(30)	NOT NULL,
	Phone			nvarchar(15)	NOT NULL,
	DOB				date			NOT NULL,
	Gender			nvarchar(10)	NOT NULL,
	Balance			decimal(10, 2)	NOT NULL,
	ChildProofCap	nvarchar(10)	NOT NULL,
	HeadHH			nvarchar(50)	NOT NULL,
	Allergies		nvarchar(90)	NOT NULL,
	constraint pk_tblCustomer primary key clustered (CustID)
)
;
go

EXEC sp_help 'Marketing.tblCustomer'; 
go

/***** Table No. 4 - Pharmacy.tblClinic ****/
if OBJECT_ID('Pharmacy.tblClinic','U')
is not null drop table Pharmacy.tblClinic
create table Pharmacy.tblClinic
(
	ClinicID	nvarchar(20)	NOT NULL,
	ClinicName	nvarchar(50)	NOT NULL,
	Address1	nvarchar(60)	NOT NULL,
	Address2	nvarchar(60)	NULL,
	City		nvarchar(15)	NOT NULL,
	Province	nchar(2)		NOT NULL,
	PostalCode	nchar(6)		NOT NULL,
	Phone		nvarchar(15)	NOT NULL,
	constraint pk_tblClinic primary key clustered (ClinicID)
)
;
go

EXEC sp_help 'Pharmacy.tblClinic'; 
go

/***** Table No. 5 - Pharmacy.tblDoctor ****/
if OBJECT_ID('Pharmacy.tblDoctor','U')
is not null drop table Pharmacy.tblDoctor
create table Pharmacy.tblDoctor
(
	DoctorID	nvarchar(20)		NOT NULL,
	ClinicID	nvarchar(20)		NOT NULL,
	DoctorFist	nvarchar(30)		NOT NULL,
	DoctorLast	nvarchar(30)		NOT NULL,
	Phone		nvarchar(15)		NOT NULL,
	Cell		nvarchar(15)		NOT NULL,
	constraint pk_tblDoctor primary key clustered (DoctorID)
)
;
go

EXEC sp_help 'Pharmacy.tblDoctor'; 
go

/***** Table No. 6 - Pharmacy.tblDrug ****/
if OBJECT_ID('Pharmacy.tblDrug','U')
is not null drop table Pharmacy.tblDrug
create table Pharmacy.tblDrug
(
	DIN				nchar(8)		NOT NULL,
	Name			nvarchar(30)	NOT NULL,
	Generic			bit				NOT NULL,
	Description		nvarchar(200)	NOT NULL,
	Unit			nvarchar(10)	NOT NULL,
	Dosage			nvarchar(10)	NOT NULL,
	DosageForm		nvarchar(20)	NOT NULL,
	Cost			decimal(8, 2)	NOT NULL,
	Price			decimal(8, 2)	NOT NULL,
	Interactions	nvarchar(100)	NOT NULL,
	Supplier		nvarchar(50)	NOT NULL,
	constraint pk_tblDrug primary key clustered (DIN)
)
;
go

EXEC sp_help 'Pharmacy.tblDrug'; 
go

/***** Table No. 7 - Pharmacy.tblRx ****/
if OBJECT_ID('Pharmacy.tblRx','U')
is not null drop table Pharmacy.tblRx 
create table Pharmacy.tblRx
(
	PrescriptionID	nvarchar(20)	NOT NULL,
	DIN				nchar(8)		NOT NULL,
	CustID			nvarchar(10)	NOT NULL,
	DoctorID		nvarchar(20)	NOT NULL,
	Quantity		decimal(10, 2)	NOT NULL,
	Unit			nvarchar(10)	NOT NULL,
	Date			date			NOT NULL,
	ExpireDate		date			NOT NULL,
	Refills			nvarchar(50)	NOT NULL,
	AutoRefills		bit				NOT NULL,
	RefillUsed		nvarchar(50)	NOT NULL,
	Instructions	nvarchar(50)	NOT NULL,
	constraint pk_tblRx primary key clustered (PrescriptionID)
)
;
go

EXEC sp_help 'Pharmacy.tblRx'; 
go

/***** Table No. 8 - Pharmacy.tblRefill ****/
if OBJECT_ID('Pharmacy.tblRefill','U')
is not null drop table Pharmacy.tblRefill
create table Pharmacy.tblRefill
(
	PrescriptionID  nvarchar(20) NOT NULL,
	RefillDate		date		 NOT NULL,
	EmpID			nvarchar(10) NOT NULL,
	constraint pk_tblRefill primary key clustered (PrescriptionID,RefillDate)
)
;
go

EXEC sp_help 'Pharmacy.tblRefill'; 
go

/***** Table No. 9 - HumanResources.tblJobTitle ****/
if OBJECT_ID('HumanResources.tblJobTitle','U')
is not null drop table HumanResources.tblJobTitle
create table HumanResources.tblJobTitle
(
	JobID nvarchar(20)  NOT NULL,
	Title nvarchar(100) NOT NULL,
	constraint pk_tblJobTitle primary key clustered (JobID)
)
;
go

EXEC sp_help 'HumanResources.tblJobTitle'; 
go

/***** Table No. 10 - HumanResources.tblClass ****/
if OBJECT_ID('HumanResources.tblClass','U')
is not null drop table HumanResources.tblClass
create table HumanResources.tblClass
(
	ClassID		nvarchar(10)	NOT NULL,
	Description nvarchar(200)	NOT NULL,
	Cost		decimal(10, 2)	NOT NULL,
	Renewal		smallint		NOT NULL,
	Required	bit				NOT NULL,
	Provider	nvarchar(50)	NOT NULL,
	constraint pk_tblClass primary key clustered (ClassID)
)
;
go

EXEC sp_help 'HumanResources.tblClass'; 
go

/***** Table No. 11 - HumanResources.tblEmployee ****/
if OBJECT_ID('HumanResources.tblEmployee','U')
is not null drop table HumanResources.tblEmployee
create table HumanResources.tblEmployee
(
	EmpID		nvarchar(10)	NOT NULL,
	JobID		nvarchar(20)	NOT NULL,
	EmpFirst	nvarchar(30)	NOT NULL,
	EmpMl		nvarchar(2)		NULL,
	EmpLast		nvarchar(30)	NOT NULL,
	SINs		nchar(11)		NOT NULL,
	DOB			date			NOT NULL,
	StartDate	date			NOT NULL,
	EndDate		date			NULL,
	Address		nvarchar(60)	NOT NULL,
	City		nvarchar(15)	NOT NULL,
	Province	nchar(2)		NOT NULL,
	PostalCode	nchar(6)		NOT NULL,
	Memo		nvarchar(255)	NOT NULL,
	Phone		nvarchar(15)	NOT NULL,
	Cell		nvarchar(15)	NOT NULL,
	Salary		decimal(10, 2)	NOT NULL,
	HourlyRate	decimal(5, 2)	NOT NULL,
	Review		date			NULL,
	Comments	varchar(50)		NOT NULL,	
	constraint pk_tblEmployee primary key clustered (EmpID)
)
;
go

EXEC sp_help 'HumanResources.tblEmployee'; 
go

/***** Table No. 12 - HumanResources.tblEmployeeTraining ****/
if OBJECT_ID('HumanResources.tblEmployeeTraining','U') is not null 
	drop table HumanResources.tblEmpTraining
create table HumanResources.tblEmployeeTraining
(
	EmpID		nvarchar(10) NOT NULL,
	ClassID		nvarchar(10) NOT NULL,
	Date		date		 NOT NULL,
	constraint pk_tblEmployeeTraining primary key clustered (EmpID,ClassID,Date)
)
;
go

EXEC sp_help 'HumanResources.tblEmployeeTraining'; 
go