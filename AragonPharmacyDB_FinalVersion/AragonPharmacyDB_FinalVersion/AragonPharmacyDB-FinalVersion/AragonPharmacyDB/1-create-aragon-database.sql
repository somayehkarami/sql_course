/* Purpose : create the database AragonPharmacyDB
	Script Date: February 8, 2021
	Developed by : W.L
*/

/* add a statement that specifieds the script
runs in the context of the master database */

-- switch to the master database
-- user database_name
use master
;
go -- include end of the batch marker

/* Partial syntax to create a database
create object_type object_name
create database database_name
*/

/* Create database AragonPharmacyDB */
create database AragonPharmacyDB
on primary
(
	name = 'AragonPharmacyDB',
	size = 12MB,
	filegrowth = 10MB,
	maxsize = 500MB,
	filename = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\AragonPharmacyDB.mdf'
)
log on
(
	name = 'AragonPharmacyDB_log',
	size = 3MB,
	filegrowth = 10%,
	maxsize = 25MB,
	filename = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\AragonPharmacyDB_log.ldf'
)
;
go

/* return information about the Aragon Pharmacy database using system stored procedure. */
execute sp_helpdb 'AragonPharmacyDB'
;
go