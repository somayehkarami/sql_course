/* Purpose : create the database AragonPharmacyDB
	Script Date: February 14, 2021
	Developed by : W.L
*/

/* add a statement that specifieds the script
runs in the context of the master database */

-- switch to the Aragon Pharmacy database
use AragonPharmacyDB;
go -- include end of the batch marker

/***** Table No. 1 - Marketing.tblHealthPlan ****/
bulk insert Marketing.tblHealthPlan
from 'C:\inputdata\HealthPlan.csv'  
with
(
	FirstRow = 2,
	FieldTerminator = ',',
	RowTerminator = '\n',
	TABLOCK
)
;
go

select *  from Marketing.tblHealthPlan
;
go

/***** Table No. 2 - Marketing.tblHousehold ****/
bulk insert Marketing.tblHousehold
from 'C:\inputdata\Household.csv'  
with
(
	FirstRow = 2,
	FieldTerminator = ',',
	RowTerminator = '\n',
	TABLOCK
)
;
go

select *  from Marketing.tblHousehold
;
go

/***** Table No. 3 - Marketing.tblCustomer ****/
bulk insert Marketing.tblCustomer
from 'C:\inputdata\Customer.csv'  
with
(
	FirstRow = 2,
	FieldTerminator = ',',
	RowTerminator = '\n',
	TABLOCK
)
;
go

select *  from Marketing.tblCustomer
;
go

/***** Table No. 4 - Pharmacy.tblClinic ****/
bulk insert Pharmacy.tblClinic
from 'C:\inputdata\Clinic.csv'  
with
(
	FirstRow = 2,
	FieldTerminator = ',',
	RowTerminator = '\n',
	TABLOCK
)
;
go

select *  from Pharmacy.tblClinic
;
go

/***** Table No. 5 - Pharmacy.tblDoctor ****/
bulk insert Pharmacy.tblDoctor
from 'C:\inputdata\Doctor.csv'  
with
(
	FirstRow = 2,
	FieldTerminator = ',',
	RowTerminator = '\n',
	TABLOCK
)
;
go

select *  from Pharmacy.tblDoctor
;
go

/***** Table No. 6 - Pharmacy.tblDrug ****/
bulk insert Pharmacy.tblDrug
from 'C:\inputdata\Drug.csv'  
with
(
	FirstRow = 2,
	FieldTerminator = ',',
	RowTerminator = '\n',
	TABLOCK
)
;
go

select *  from Pharmacy.tblDrug
;
go

/***** Table No. 7 - Pharmacy.tblRx ****/
bulk insert Pharmacy.tblRx
from 'C:\inputdata\Rx.csv'  
with
(
	FirstRow = 2,
	FieldTerminator = ',',
	RowTerminator = '\n',
	TABLOCK
)
;
go

select *  from Pharmacy.tblRx
;
go

/***** Table No. 8 - Pharmacy.tblRefill ****/
bulk insert Pharmacy.tblRefill
from 'C:\inputdata\Refill.csv'  
with
(
	FirstRow = 2,
	FieldTerminator = ',',
	RowTerminator = '\n',
	TABLOCK
)
;
go

select *  from Pharmacy.tblRefill
;
go

/***** Table No. 9 - HumanResources.tblJobTitle ****/
bulk insert HumanResources.tblJobTitle
from 'C:\inputdata\JobTitle.csv'  
with
(
	FirstRow = 2,
	FieldTerminator = ',',
	RowTerminator = '\n',
	TABLOCK
)
;
go

select *  from HumanResources.tblJobTitle
;
go

/***** Table No. 10 - HumanResources.tblClass ****/
bulk insert HumanResources.tblClass
from 'C:\inputdata\Class.csv'  
with
(
	FirstRow = 2,
	FieldTerminator = ',',
	RowTerminator = '\n',
	TABLOCK
)
;
go

select *  from HumanResources.tblClass
;
go

/***** Table No. 11 - HumanResources.tblEmployee ****/
bulk insert HumanResources.tblEmployee
from 'C:\inputdata\Employee.csv'  
with
(
	FirstRow = 2,
	FieldTerminator = ',',
	RowTerminator = '\n',
	TABLOCK
)
;
go

select *  from HumanResources.tblEmployee
;
go

/***** Table No. 12 - HumanResources.tblEmployeeTraining ****/
bulk insert HumanResources.tblEmployeeTraining
from 'C:\inputdata\EmployeeTraining.csv'  
with
(
	FirstRow = 2,
	FieldTerminator = ',',
	RowTerminator = '\n',
	TABLOCK
)
;
go

select *  from HumanResources.tblEmployeeTraining
;
go