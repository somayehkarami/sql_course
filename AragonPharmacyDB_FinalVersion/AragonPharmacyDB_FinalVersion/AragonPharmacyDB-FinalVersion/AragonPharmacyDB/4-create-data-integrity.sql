/* Purpose : add the data integrity to the tables of database AragonPharmacyDB
	Script Date: February 8, 2021
	Developed by : W.L
*/

/* add a statement that specifieds the script
runs in the context of the master database */

-- switch to the Aragon Pharmacy database
use AragonPharmacyDB;
go -- include end of the batch marker

/* ********************************************* */
/* add foreign key constaint(s)                  */
/* ********************************************* */

/* 1. add foreign key to table tblCustomer       */
-- 1.1 between  tblCustomer and tblHealthPlan
alter table Marketing.tblCustomer
	add constraint fk_tblCustomer_tblHealthPlan foreign key(PlanID)
		references Marketing.tblHealthPlan(PlanID)
;
go
-- 1.2 between  tblCustomer and tblHousehold
alter table Marketing.tblCustomer
	add constraint fk_tblCustomer_tblHousehold foreign key(HouseID)
		references Marketing.tblHousehold(HouseID)
;
go

/* 2. add foreign key to table tblDoctor        */
-- 2.1 between  tblDoctor and tblClinic
alter table Pharmacy.tblDoctor
	add constraint fk_tblDoctor_Clinic foreign key(ClinicID)
		references Pharmacy.tblClinic(ClinicID)
;
go

/* 3. add foreign key to table tblRx           */
-- 3.1 between tblRx and tblDoctor
alter table Pharmacy.tblRx
	add constraint fk_tblRx_tblDoctor foreign key(DoctorID)
		references Pharmacy.tblDoctor(DoctorID)
;
go
-- 3.2 between tblRx and tblDrug
alter table Pharmacy.tblRx
	add	constraint fk_tblRx_tblDrug foreign key(DIN)
		references Pharmacy.tblDrug(DIN)
;
go

-- 3.3 between tblRx and tblCustomer
alter table Pharmacy.tblRx
	add	constraint fk_tblRx_tblCustomer foreign key(CustID)
		references Marketing.tblCustomer(CustID)
;
go

/* 4. add foreign key to table tblRefill        */
-- 4.1 between tblRx and tblRefill
alter table Pharmacy.tblRefill
	add constraint fk_tblRefill_tblRx foreign key(PrescriptionID)
		references Pharmacy.tblRx(PrescriptionID);
go
-- 4.2 between tblRefill and tblEmployee
alter table Pharmacy.tblRefill
	add constraint fk_tblRefill_tblEmployee foreign key(EmpID)
		references HumanResources.tblEmployee(EmpID);
go
/* 5. add foreign key to table tblEmployee        */
-- 5.1 between tblEmployee and tblJobTitle
alter table HumanResources.tblEmployee
	add constraint fk_tblEmployee_tblJobTitle foreign key(JobID)
		references HumanResources.tblJobTitle(JobID);
go
/* 6. add foreign key to table tblEmployeeTraining */
-- 6.1 between tblEmployeeTraining and tblEmployee
alter table HumanResources.tblEmployeeTraining
	add constraint fk_tblEmployeeTraining_tblEmployee foreign key(EmpID)
		references HumanResources.tblEmployee(EmpID);
go
-- 6.2 between tblEmployeeTraining and tblClass
alter table HumanResources.tblEmployeeTraining
	add constraint fk_tblEmployeeTraining_tblClass foreign key(ClassID)
		references HumanResources.tblClass(ClassID);
go
/* ********************************************* */
/* set the unique value                          */
/* ********************************************* */
-- Table No. 1 - Marketing.tblHealthPlan
alter table Marketing.tblHealthPlan
	add constraint uq_tblHealthPlan Unique(PlanID,PlanName);
go

-- Table No. 2 - Pharmacy.tblClinic
alter table Pharmacy.tblClinic
	add constraint uq_tblClinic Unique(ClinicName);
go

-- Table No. 3 - HumanResources.tblEmployee
alter table HumanResources.tblEmployee
	add constraint uq_tblEmployee Unique(SINs);
go

/* ********************************************* */
/* set the default value                         */
/* ********************************************* */
-- Table No. 1 - Pharmacy.tblDrug
alter table Pharmacy.tblDrug
	add constraint df_Cost  default(0.00)  for Cost;
go

alter table Pharmacy.tblDrug
	add constraint df_Price default(0.00)  for Price;
go

-- Table No. 2 - HumanResources.tblEmployee
alter table HumanResources.tblEmployee
	add constraint df_Salary     default(0.00) for Salary;
go

alter table HumanResources.tblEmployee
	add constraint df_HourlyRate default(0.00) for HourlyRate;
go

-- Table No. 3 - HumanResources.tblClass
alter table HumanResources.tblClass
	add constraint df_Cost default(0.00) for Cost;
go

alter table HumanResources.tblClass
	add constraint df_Renewal default(0) for Renewal;
go

/* ********************************************* */
/* set the check value                           */
/* ********************************************* */
-- Table No. 1 - Marketing.tblHealthPlan
alter table Marketing.tblHealthPlan
	add constraint ck_PostalCode_tblHealthPlan check(PostalCode like '[A-Z][0-9][A-Z][0-9][A-Z][0-9]');
go

alter table Marketing.tblHealthPlan
	add constraint ck_Province_tblHealthPlan check(Province IN('QC','BC','AB','MB','NB','NL','NT','NS','NU','ON','PE','SK','YT'));
go

alter table Marketing.tblHealthPlan
	add constraint ck_Phone_tblHealthPlan check(Phone like '%[0-9][0-9][0-9]%[0-9][0-9][0-9][-][0-9][0-9][0-9][0-9]');
go

-- Table No. 2 - Marketing.tblCustomer
alter table Marketing.tblCustomer
	add constraint ck_Phone_tblCustomer check(Phone like '%[0-9][0-9][0-9]%[0-9][0-9][0-9][-][0-9][0-9][0-9][0-9]');
go

alter table Marketing.tblCustomer
	add constraint ck_DOB_tblCustomer check(DOB < getdate());
go

alter table Marketing.tblCustomer
	add constraint ck_CustID_tblCustomer check(CustID like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'); -- 10 digits
go

-- Table No. 3 - Marketing.tblHousehold
alter table Marketing.tblHousehold
	add constraint ck_Province_tblHousehold   check(Province IN('QC','BC','AB','MB','NB','NL','NT','NS','NU','ON','PE','SK','YT'));

alter table Marketing.tblHousehold
	add constraint ck_PostalCode_tblHousehold check(PostalCode like '[A-Z][0-9][A-Z][0-9][A-Z][0-9]');
go

-- Table No. 4 - Pharmacy.tblDoctor
alter table Pharmacy.tblDoctor
	add constraint ck_Phone_tblDoctor check(Phone like '%[0-9][0-9][0-9]%[0-9][0-9][0-9][-][0-9][0-9][0-9][0-9]');
go

alter table Pharmacy.tblDoctor
	add constraint ck_DoctorID_tblDoctor check(CustID like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'); -- 10 digits
go

-- Table No. 5 - Pharmacy.tblClinic
alter table Pharmacy.tblClinic
	add constraint ck_Province_tblClinic check(Province IN('QC','BC','AB','MB','NB','NL','NT','NS','NU','ON','PE','SK','YT'));

alter table Pharmacy.tblClinic
	add constraint ck_PostalCode_tblClinic check(PostalCode like '[A-Z][0-9][A-Z][0-9][A-Z][0-9]');
go
alter table Pharmacy.tblClinic
	add constraint ck_Phone_tblClinic check(Phone like '%[0-9][0-9][0-9]%[0-9][0-9][0-9][-][0-9][0-9][0-9][0-9]');
go

-- Table No. 6 - Pharmacy.tblRx
alter table Pharmacy.tblRx
	add constraint ck_ExpireDate_tblRx check(ExpireDate > Date);
go

alter table Pharmacy.tblRx
	add constraint ck_CustID_tblRx check(CustID like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'); -- 10 digits
go

alter table Pharmacy.tblRx
	add constraint ck_DoctorID_tblRx check(DoctorID like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'); -- 10 digits
go

-- Table No. 7 - Pharmacy.tblDrug
alter table Pharmacy.tblDrug
	add constraint ck_DIN_tblDrug check(DIN like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]');
go

alter table Pharmacy.tblDrug
	add constraint ck_Price_tblDrug check(Price > cost);
go

-- Table No. 8 - HumanResources.tblEmployee
alter table HumanResources.tblEmployee
	add constraint ck_province_tblEmployee   check(Province IN('QC','BC','AB','MB','NB','NL','NT','NS','NU','ON','PE','SK','YT'));
go

alter table HumanResources.tblEmployee
	add constraint ck_DOB_tblEmployee		 check(DOB < getdate());
go

alter table HumanResources.tblEmployee
	add constraint ck_PostalCode_tblEmployee check(PostalCode like '[A-Z][0-9][A-Z][0-9][A-Z][0-9]');
go

alter table HumanResources.tblEmployee
	add constraint ck_Phone_tblEmployee		 check(Phone like '%[0-9][0-9][0-9]%[0-9][0-9][0-9][-][0-9][0-9][0-9][0-9]');
go

alter table HumanResources.tblEmployee
	add constraint ck_EndDate_tblEmployee	 check(EndDate > StartDate or EndDate is Null);
go

alter table HumanResources.tblEmployee
	add constraint ck_SINs_tblEmployee		 check(SINs like '[0-9][0-9][0-9][-][0-9][0-9][0-9][-][0-9][0-9][0-9]');
go