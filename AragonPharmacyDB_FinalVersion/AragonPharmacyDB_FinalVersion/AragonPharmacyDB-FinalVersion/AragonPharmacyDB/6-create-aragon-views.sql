/* Purpose : create the views of database AragonPharmacyDB
	Script Date: February 15, 2021
	Developed by : W.L
*/

/* add a statement that specifieds the script
runs in the context of the master database */

-- switch to the Aragon Pharmacy database
use AragonPharmacyDB;
go -- include end of the batch marker

/***** View No. 1 - HourlyRateAnalysisView ****/
-- list the wages for pharmacy technicians and cashiers, ranked from highest to lowest
IF  OBJECT_ID('HumanResources.HourlyRateAnalysisView','V') is not null
	DROP VIEW HumanResources.HourlyRateAnalysisView
;
go 

create view HumanResources.HourlyRateAnalysisView
as
select 
	 E.EmpID                                                   as 'Employee ID',
	 JT.Title                                                  as 'Employee job position',
	 CONCAT_WS(' ',E.EmpFirst,coalesce(E.EmpMl,''),E.EmpLast)  as 'Employee Full Name',
	 E.HourlyRate                                              as 'HourlyRate Of Technicians and cashiers', -- non-salaried employees
	 E.SINs                                                    as 'Employee SIN Number',
	 CONVERT(nvarchar,E.DOB,101)                               as 'Day of Birthday',
	 CONVERT(nvarchar,E.StartDate,101)                         as 'Employee hireDate',
	 CONCAT_WS(',',E.Address,E.City,E.Province,E.PostalCode)   as 'Employee address',
	 E.Memo                                                    as 'Employee memo',
	 E.Phone                                                   as 'Employee Phone',
	 E.Cell                                                    as 'Employee Cell',
	 CONVERT(nvarchar,E.Review,101)                            as 'Review Date',
	 E.Comments                                                as 'Employee Speaking Language'
from   HumanResources.tblEmployee as E
	inner join HumanResources.tblJobTitle JT 
		on	E.JobID = JT.JobID
			and  E.HourlyRate != 0.00
			and  E.EndDate is null 
order by E.HourlyRate desc offset 0 rows    -- the date ranked from highest to lowest
;
go

execute sp_helptext 'HumanResources.HourlyRateAnalysisView'
;
go

-- get the date info. 
select * from  HumanResources.HourlyRateAnalysisView
;
go

/***** View No. 2 - SpeakSpanishView ****/
-- list the employees who speaks Spanish (or any other language besides English and French)
IF  OBJECT_ID('HumanResources.SpeakSpanishView','V') is not null
	DROP VIEW HumanResources.SpeakSpanishView
;
go 

create view HumanResources.SpeakSpanishView
as
select 
	 E.EmpID                                                   as 'Employee ID',
	 JT.Title                                                  as 'Employee job position',
	 CONCAT_WS(' ',E.EmpFirst,coalesce(E.EmpMl,''),E.EmpLast)  as 'Employee Full Name',
	 E.Comments                                                as 'Employee Speaking Language',
	 E.Salary												   as 'Employee Salary',                        -- salaried employees
	 E.HourlyRate                                              as 'HourlyRate Of Technicians and cashiers', -- non-salaried employees
	 E.SINs                                                    as 'Employee SIN Number',
	 CONVERT(nvarchar,E.DOB,101)                               as 'Day of Birthday',
	 CONVERT(nvarchar,E.StartDate,101)                         as 'Employee hireDate',
	 CONCAT_WS(' ',E.Address,E.City,E.Province,E.PostalCode)   as 'Employee address',
	 E.Memo                                                    as 'Employee memo',
	 E.Phone                                                   as 'Employee Phone',
	 E.Cell                                                    as 'Employee Cell',
	 CONVERT(nvarchar,E.Review,101)                            as 'Review Date'
from   HumanResources.tblEmployee as E
	inner join HumanResources.tblJobTitle JT 
		on	E.JobID = JT.JobID
			and  E.EndDate  is null 
			and	 E.Comments not in('French-speaking','English-speaking')   -- Spanish speaker or any other language speaker besides English and French employee		
;
go

-- return the information about the view
execute sp_helptext 'HumanResources.SpeakSpanishView'
;
go

-- get the date info. 
select * from  HumanResources.SpeakSpanishView
;
go 

/***** View No. 3 - HourlyRateSummaryView ****/
-- Returns the hourly rate summary that returns the minimum, and maximum of hourly rate related to the job position.
IF  OBJECT_ID('HumanResources.HourlyRateSummaryView','V') is not null
	DROP  VIEW HumanResources.HourlyRateSummaryView
;
go 
create view HumanResources.HourlyRateSummaryView
as
select 
	 max(E.HourlyRate)                                         as 'Maximum of hourly rate',
	 min(E.HourlyRate)                                         as 'Minimum of hourly rate',
	 E.JobID                                                   as 'Employee job position'
from   HumanResources.tblEmployee as E
where  E.EndDate is null and E.HourlyRate is not null
group by E.JobID
;
go

-- return the information about the view
execute sp_helptext 'HumanResources.HourlyRateSummaryView'
;
go

-- get the date info. 
select * from  HumanResources.HourlyRateSummaryView
;
go