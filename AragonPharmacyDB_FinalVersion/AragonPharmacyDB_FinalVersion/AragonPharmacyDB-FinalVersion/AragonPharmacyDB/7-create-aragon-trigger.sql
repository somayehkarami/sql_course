/* Purpose : create the Triggers of database AragonPharmacyDB
	Script Date: February 16, 2021
	Developed by : W.L
*/

/* add a statement that specifieds the script
runs in the context of the master database */

-- switch to the Aragon Pharmacy database
use AragonPharmacyDB;
go -- include end of the batch marker

/***** trigger No. 1 - Marketing.tblHealthPlanTrigger ****/
create trigger tblHealthPlanTrigger
on Marketing.tblHealthPlan
after insert, update
as 
	begin
		update Marketing.tblHealthPlan
		set Days = ABS(Days), 
			PostalCode = upper(PostalCode),
			Province   = upper(Province)
		where PlanID in (select PlanID from inserted) -- insert,delete
	end
;
go 

/***** trigger No. 2 - Pharmacy.tblClinic ****/
create trigger tblClinicTrigger
on Pharmacy.tblClinic
after insert, update
as 
	begin
		update Pharmacy.tblClinic
		set PostalCode = upper(PostalCode),
			Province   = upper(Province)
		where ClinicID in (select ClinicID from inserted) -- insert,delete
	end
;
go 


/***** trigger No. 3 - HumanResources.tblEmployee ****/
create trigger tblEmployeeTrigger
on HumanResources.tblEmployee
after insert, update
as 
	begin
		update HumanResources.tblEmployee
		set PostalCode = upper(PostalCode),
			Province   = upper(Province)
		where EmpID in (select EmpID from inserted) -- insert,delete
	end
;
go 