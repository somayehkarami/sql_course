/* Purpose : create the schema of database AragonPharmacyDB
	Script Date: February 8, 2021
	Developed by : W.L
*/

/* add a statement that specifieds the script
runs in the context of the master database */

-- switch to the Aragon Pharmacy database
use AragonPharmacyDB;
go -- include end of the batch marker


/* create schema and set the owner to each of them
1. Pharmacy
2. Marketing
3. Human Resours
*/

-- 1. create Pharmacy schema
create schema Pharmacy authorization DBO
;
go

-- 2. create Marketing schema
create schema Marketing authorization DBO
;
go

-- 3. create HumanResources schema
create schema HumanResources authorization DBO
;
go