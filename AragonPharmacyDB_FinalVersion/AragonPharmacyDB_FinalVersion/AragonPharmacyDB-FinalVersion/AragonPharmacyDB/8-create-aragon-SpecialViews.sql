/* Purpose : create the special views of database AragonPharmacyDB
	Script Date: February 16, 2021
	Developed by : W.L
*/

/* add a statement that specifieds the script
runs in the context of the master database */

-- switch to the Aragon Pharmacy database
use AragonPharmacyDB;
go -- include end of the batch marker

/* ************************************************************************************** */
/* task 01 - create a carpool list identifying those employees who live in the same city. */
/* ************************************************************************************** */
IF  OBJECT_ID('HumanResources.EmployeeSameCityView','V') is not null
	DROP VIEW  HumanResources.EmployeeSameCityView
;
go 
create view HumanResources.EmployeeSameCityView
as
select
	 E.EmpID                                                   as 'Employee ID',
	 JT.Title                                                  as 'Employee job position',
	 CONCAT_WS(' ',E.EmpFirst,coalesce(E.EmpMl,''),E.EmpLast)  as 'Employee Full Name',
	 E.Comments                                                as 'Employee Speaking Language',
	 E.Salary												   as 'Employee Salary',                        -- salaried employees
	 E.HourlyRate                                              as 'HourlyRate Of Technicians and cashiers', -- non-salaried employees
	 E.SINs                                                    as 'Employee SIN Number',
	 CONVERT(nvarchar,E.DOB,101)                               as 'Day of Birthday',
	 CONVERT(nvarchar,E.StartDate,101)                         as 'Employee hireDate',
	 CONCAT_WS(',',E.Address,E.City,E.Province,E.PostalCode)   as 'Employee address',
	 E.Memo                                                    as 'Employee memo',
	 E.Phone                                                   as 'Employee Phone',
	 E.Cell                                                    as 'Employee Cell',
	 CONVERT(nvarchar,E.Review,101)                            as 'Review Date'
from   HumanResources.tblEmployee as E
	inner join HumanResources.tblJobTitle JT 
		on	E.JobID = JT.JobID
			and  E.EndDate  is null 
order by E.City asc offset 0 rows 
;
go

execute sp_helptext 'HumanResources.EmployeeSameCityView'
;
go

-- get the date info. about the view
select * from  HumanResources.EmployeeSameCityView
;
go


-- In case the manager wants to specify which city employees live in, we also have the second methode as the following, to create a procedure to realize this request 

IF OBJECT_ID('HumanResources.getEmployeeFromSameCitySp' ) is not null
	Drop Procedure HumanResources.getEmployeeFromSameCitySp
;
go
CREATE PROCEDURE HumanResources.getEmployeeFromSameCitySp
	(
	@City as nvarchar(15) 
	)
As
	Begin
		Select EmpID , EmpFirst , City
		From HumanResources.tblEmployee
		Where City like @City 
		And EndDate is null
	End
;
go

--testing if manager wants to know all the employees who live in 'Laval'
Exec HumanResources.getEmployeeFromSameCitySp 'Laval'

--testing if manager wants to know all the employees who live in 'Montreal'
Exec HumanResources.getEmployeeFromSameCitySp 'Montreal'


/* ************************************** */
/* task 02 -  Avoiding Duplicate Records. */
/* ************************************** */
IF  OBJECT_ID('Marketing.CustomerHeadHHView','V') is not null
	DROP VIEW Marketing.CustomerHeadHHView
;
go 
create view Marketing.CustomerHeadHHView
as
select
	   CONCAT_WS(' ',C.CustFisrt,C.CustLast)    as 'Customer Full Name',
	   C.Phone                                  as 'Phone number',
	   CONVERT(nvarchar,C.DOB,101)              as 'Day of Birthday', 
	   C.Gender                                 as 'Gender',
	   C.Balance                                as 'Balance',
	   C.Allergies                              as 'Allergies',
	   'YES'                                  as 'The head of household',
	   CONCAT_WS(',',H.Address,H.City,H.Province,H.PostalCode)   as 'Customer address'
from   Marketing.tblCustomer AS C
	inner join Marketing.tblHousehold AS H
		ON  C.HouseID = H.HouseID
			and C.HeadHH = '1'           -- setting a criterion that HeadHH is Yes to avoid duplicate records
;
go

execute sp_helptext 'Marketing.CustomerHeadHHView'
;
go

-- get the date info. about the view
select * from  Marketing.CustomerHeadHHView
;
go
/* *************************************************** */
/* task 03 -  Using Queries to Find Unmatched Records. */
/* *************************************************** */
IF  OBJECT_ID('HumanResources.EmployeeNotTrainingView','V') is not null
	DROP VIEW HumanResources.EmployeeNotTrainingView
;
go 
create view HumanResources.EmployeeNotTrainingView
as
select
	 E.EmpID                                                   as 'Employee ID',
	 JT.Title                                                  as 'Employee job position',
	 CONCAT_WS(' ',E.EmpFirst,coalesce(E.EmpMl,''),E.EmpLast)  as 'Employee Full Name',
	 E.Comments                                                as 'Employee Speaking Language',
	 E.Salary												   as 'Employee Salary',                        -- salaried employees
	 E.HourlyRate                                              as 'HourlyRate Of Technicians and cashiers', -- non-salaried employees
	 E.SINs                                                    as 'Employee SIN Number',
	 CONVERT(nvarchar,E.DOB,101)                               as 'Day of Birthday',
	 CONVERT(nvarchar,E.StartDate,101)                         as 'Employee hireDate',
	 CONCAT_WS(',',E.Address,E.City,E.Province,E.PostalCode)   as 'Employee address',
	 E.Memo                                                    as 'Employee memo',
	 E.Phone                                                   as 'Employee Phone',
	 E.Cell                                                    as 'Employee Cell',
	 CONVERT(nvarchar,E.Review,101)                            as 'Review Date'
from   HumanResources.tblEmployee as E
	inner join HumanResources.tblJobTitle JT 
		on	E.JobID = JT.JobID
			and  E.EndDate  is null 
	left join HumanResources.tblEmployeeTraining ET
		on  E.EmpID = ET.EmpID
where ET.EmpID is null
;
go

execute sp_helptext 'HumanResources.EmployeeNotTrainingView'
;
go

-- get the date info.
select * from  HumanResources.EmployeeNotTrainingView
;
go

/* ********************************************************************************* */
/* task 04 -  Using Parameter Values (create multi-statement table-valued function). */
/* ********************************************************************************* */
DROP FUNCTION IF EXISTS HumanResources.getSubstituteListFn 
;
go 
create function HumanResources.getSubstituteListFn(@jobID nvarchar)
returns @getSubstituteList table
(
	EmpLast		nvarchar(30)	NOT NULL,
	EmpFirst	nvarchar(30)	NOT NULL,
	Phone		nvarchar(15)	NOT NULL,
	Cell		nvarchar(15)	NOT NULL,
	JobID		nvarchar(20)	NOT NULL,
	EndDate		date			NULL
)
as
begin
with emp_cte(EmpLast,EmpFirst,Phone,Cell,JobID,EndDate)
	as 
	(
		select E.EmpLast,E.EmpFirst,E.Phone,E.Cell,E.JobID, CONVERT(nvarchar,E.EndDate,101)
		from   HumanResources.tblEmployee as E
		where  E.JobID = @jobID and E.EndDate is null
	)
-- copy the required columns to the result of the function
	insert @getSubstituteList
	select EmpLast,EmpFirst,Phone,Cell,JobID,CONVERT(NCHAR(10),EndDate,101) 
	from   emp_cte
	return
end;
go

-- get the date info. from the function
select EmpLast,EmpFirst,Phone,Cell,JobID,EndDate 
from   HumanResources.getSubstituteListFn(4)
;
go


-- the seconde method to create this function 'HumanResources.getSubstituteListFn' by using scalar function

DROP FUNCTION IF EXISTS HumanResources.HumanResources.getSubstituteListFNmethod2
;
go 
create function HumanResources.getSubstituteListFNmethod2
	(
		@JobID as int
	)
returns table
as
	return
		select EmpLast, EmpFirst, Phone, Cell, JobID, EndDate
		from HumanResources.tblEmployee
		where JobID = @JobID 
				and EndDate is null
;
go



/* ************************************************** */
/* task 05 -  Analyzing Data from More Than One Table */
/* ************************************************** */
-- ---------------------------------------------------------------------------------------------------------
-- View 01 :  EmployeeClassesView (producing a list of employees who have taken certification classes)
-- ---------------------------------------------------------------------------------------------------------
IF  OBJECT_ID('HumanResources.EmployeeClassesView','V') is not null
	DROP VIEW HumanResources.EmployeeClassesView 
;
go

create view HumanResources.EmployeeClassesView
as
select E.EmpID								as 'Employee ID',
	   CONCAT_WS(' ',E.EmpFirst,E.EmpLast)  as 'Employee Full Name',
	   ET.ClassID                           as 'Class ID',
	   convert(nchar(10),ET.Date,101)       as 'Employee Training Date'
from   HumanResources.tblEmployee as E
	inner join HumanResources.tblEmployeeTraining ET
		on  E.EmpID = ET.EmpID
			and E.EndDate is null 
;
go

execute sp_helptext 'HumanResources.EmployeeClassesView'
;
go

-- get the date info.
select * from HumanResources.EmployeeClassesView
;
go
-- ---------------------------------------------------------------------------------------------------------
-- View 02 :  EmployeeClassesDescriptionView (producing a list class descriptions instead of class IDs)
-- ---------------------------------------------------------------------------------------------------------
IF  OBJECT_ID('HumanResources.EmployeeClassesDescriptionView','V') is not null
	DROP VIEW HumanResources.EmployeeClassesDescriptionView
;
go 
create view HumanResources.EmployeeClassesDescriptionView
as
select E.EmpID								as 'Employee ID',
	   CONCAT_WS(' ',E.EmpFirst,E.EmpLast)  as 'Employee Full Name',
	   ET.ClassID                           as 'Class ID',
	   C.Description                        as 'Class Description',
	   convert(nchar(10),ET.Date,101)       as 'Employee Training Date'
from   HumanResources.tblEmployee as E
	inner join HumanResources.tblEmployeeTraining ET
		on  E.EmpID = ET.EmpID 
			and E.EndDate is null 
	inner join HumanResources.tblClass as C
		on  ET.ClassID = C.ClassID
;
go

execute sp_helptext 'HumanResources.EmployeeClassesDescriptionView'
;
go

-- get the date info.
select * from HumanResources.EmployeeClassesDescriptionView
;
go

-- ----------------------------------------------------------------------------------------------------------------
-- View 03 :  UpToDateView (base a new query on EmployeeTrainingView to see if employees taking required classes)
-- ----------------------------------------------------------------------------------------------------------------
IF  OBJECT_ID('HumanResources.UpToDateView','V') is not null
	DROP VIEW HumanResources.UpToDateView
;
go 
create view HumanResources.UpToDateView
as
select E.EmpID								as 'Employee ID',
	   CONCAT_WS(' ',E.EmpFirst,E.EmpLast)  as 'Employee Full Name',
	   convert(nchar(10),ET.Date,101)       as 'Employee Training Date',
	   C.Required                           as 'Employee Training Required',
	   ET.ClassID                           as 'Class ID',
	   C.Description                        as 'Class Description'
from   HumanResources.tblEmployee as E
	left outer join  HumanResources.tblEmployeeTraining ET
		on  E.EmpID = ET.EmpID 
			and E.EndDate is null 
	inner join HumanResources.tblClass as C
		on  ET.ClassID = C.ClassID
;
go

execute sp_helptext 'HumanResources.UpToDateView'
;
go

-- get the date info.
select * from HumanResources.UpToDateView
;
go

-- ----------------------------------------------------------------------------------------------------------------
-- View 04 : Required classes for pharmacy employees(select only those employees who have taken these courses)
-- ----------------------------------------------------------------------------------------------------------------
IF  OBJECT_ID('HumanResources.EmployeePastYearClassesView','V') is not null
	DROP VIEW HumanResources.EmployeePastYearClassesView
;
go 
create view HumanResources.EmployeePastYearClassesView
as
select E.EmpID								as 'Employee ID',
	   CONCAT_WS(' ',E.EmpFirst,E.EmpLast)  as 'Employee Full Name',
	   ET.ClassID                           as 'Class ID',
	   C.Description                        as 'Class Description',
	   convert(nchar(10),ET.Date,101)       as 'Employee Training Date'
from   HumanResources.tblEmployee as E
	left outer join  HumanResources.tblEmployeeTraining ET
		on  E.EmpID = ET.EmpID 
			and E.EndDate is null 
	inner join HumanResources.tblClass as C
		on  ET.ClassID = C.ClassID
			and  C.ClassID in('1','3')
		    and  ET.Date   between '1/1/2019' and '12/31/2019'
;
go

execute sp_helptext 'HumanResources.EmployeePastYearClassesView'
;
go

-- get the date info.
select * from HumanResources.EmployeePastYearClassesView
;
go

/* ************************************************** */
/* task 06 -  Calculating Statistical Information     */
/* ************************************************** */
-- --------------------------------------------------------------------------------------------------------------
-- View 01 :  MaxMinAvgHourlyRateView (calculate the minimum, maximum, and average hourly rates for each job ID.)
-- --------------------------------------------------------------------------------------------------------------
IF  OBJECT_ID('HumanResources.MaxMinAvgHourlyRateView','V') is not null
	DROP VIEW HumanResources.MaxMinAvgHourlyRateView 
;
go

create view HumanResources.MaxMinAvgHourlyRateView
as
select 
	 max(E.HourlyRate)                                         as 'Maximum of hourly rate',
	 min(E.HourlyRate)                                         as 'Minimum of hourly rate',
	 cast(avg(E.HourlyRate) as decimal(5,2))                   as 'Avarage of hourly rate',
	 E.JobID                                                   as 'Employee job position'
from   HumanResources.tblEmployee as E
where  E.EndDate is null 
	and E.HourlyRate is not null
	and E.JobID in('3','4')
group by E.JobID
;
go

execute sp_helptext 'HumanResources.MaxMinAvgHourlyRateView'
;
go

-- get the date info.
select * from HumanResources.MaxMinAvgHourlyRateView
;
go

-- --------------------------------------------------------------------------------------------------------------
-- Function 01 :  YearsOfServiceFn(calculate the years of service each employee.)
-- --------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS HumanResources.YearsOfServiceFn
;
go
create function HumanResources.YearsOfServiceFn
(
	@StartDate as date		
)
returns int
as
	begin
		declare @seniority as int	
		select  @seniority=abs(datediff(year,getdate(),@StartDate));
		return  @seniority
	end
;
go

-- get the date info. from the function
select
	   E.EmpID                                       as 'Employee ID',
	   HumanResources.YearsOfServiceFn(E.StartDate)  as 'Employee Seniority'
from   HumanResources.tblEmployee as E
where  E.EndDate is not null
;
go